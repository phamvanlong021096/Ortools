from __future__ import print_function
import sys
from ortools.constraint_solver import pywrapcp


def main():
    # Create the solver
    solver = pywrapcp.Solver("simple_example")
    # Create the variables
    num_vals = 3
    x = solver.IntVar(0, num_vals - 1, 'x')
    y = solver.IntVar(0, num_vals - 1, 'y')
    z = solver.IntVar(0, num_vals - 1, 'z')

    # create the constraint
    # solver.Add(x != y)
    # solver.Add(y != z)
    solver.Add(solver.Max(x != y, y != z) == 1)

    db = solver.Phase([x, y, z], solver.CHOOSE_FIRST_UNBOUND, solver.ASSIGN_MIN_VALUE)

    # call the solver and display the results
    solver.Solve(db)
    count = 0

    while solver.NextSolution():
        count += 1
        print("Solution", count, '\n')
        print("x = ", x.Value())
        print("y = ", y.Value())
        print("z = ", z.Value())
        print()
    print("Number of solutions:", count)


if __name__ == '__main__':
    main()
